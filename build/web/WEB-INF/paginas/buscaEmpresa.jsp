<%-- 
    Document   : buscaEmpresa
    Created on : 26/01/2017, 13:42:02
    Author     : Clayder
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        Resultado da busca:
        <ul>
            <c:forEach var="empresa" items="${empresas}">
                <li>
                    ${empresa.id}: ${empresa.nome}
                </li>
            </c:forEach>
        </ul>
    </body>
</html>
