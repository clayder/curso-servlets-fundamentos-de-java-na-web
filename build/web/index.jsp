<%-- 
    Document   : index
    Created on : 26/01/2017, 13:47:33
    Author     : Clayder
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        Bem vindo ao nosso gerenciador de empresas!
        <c:if test="${not empty usuarioLogado}">
            Você está logado como ${usuarioLogado.email} <br/>
        </c:if>

        <form action="fazTudo?tarefa=NovaEmpresa" method="POST">
            Nome: <input type="text" name="nome" /><br />
            <input type="submit" value="Enviar" />
        </form>

        <form action="login" method="POST">
            Email: <input type="text" name="email" /><br />
            Senha: <input type="password" name="senha" /><br />
            <input type="submit" value="Login" />
        </form>
        <form action="fazTudo?tarefa=Logout" method="post">
            <input type="submit" value="Logout" />
        </form>
    </body>
</html>
